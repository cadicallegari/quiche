module gitlab.com/cadicallegari/quiche

go 1.14

require (
	github.com/chi-middleware/logrus-logger v0.1.0
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-redis/redis/v8 v8.0.0-beta.7
	github.com/google/go-cmp v0.5.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/onsi/ginkgo v1.14.0 // indirect
	github.com/shopspring/decimal v1.2.0
	github.com/sirupsen/logrus v1.6.0
)
