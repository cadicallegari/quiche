package server_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/cadicallegari/quiche"
	"gitlab.com/cadicallegari/quiche/http/server"
	"gitlab.com/cadicallegari/quiche/storage"
)

func TestShouldBeHealth(t *testing.T) {
	stg, err := storage.NewLocalStorage()
	if err != nil {
		t.Fatalf("unable to perform request: %s", err)
	}

	srv := server.NewPromotionHandler(stg)

	ctx := context.TODO()
	res := httptest.NewRecorder()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "/healthz", nil)
	if err != nil {
		t.Fatalf("unable to perform request: %s", err)
	}

	srv.ServeHTTP(res, req)

	want := http.StatusOK
	if res.Code != want {
		t.Fatalf("not spected code want: %d got: %d", want, res.Code)
	}
}

func TestShouldReturnProperlyWhenNotFound(t *testing.T) {
	stg, err := storage.NewLocalStorage()
	if err != nil {
		t.Fatalf("unable to perform request: %s", err)
	}

	srv := server.NewPromotionHandler(stg)

	ctx := context.TODO()
	res := httptest.NewRecorder()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "/promotions/any-id", nil)
	if err != nil {
		t.Fatalf("unable to perform request: %s", err)
	}

	srv.ServeHTTP(res, req)

	want := http.StatusNotFound
	if res.Code != want {
		t.Fatalf("not expected code want: %d got: %d", want, res.Code)
	}
}

func TestShouldReturnProperly(t *testing.T) {
	stg, err := storage.NewLocalStorage()
	if err != nil {
		t.Fatalf("unable to perform request: %s", err)
	}

	id := "1234"

	promotion := &quiche.Promotion{ID: id}
	if err := stg.Add(context.TODO(), promotion); err != nil {
		t.Fatalf("unable to add value: %s", err)
	}

	srv := server.NewPromotionHandler(stg)

	ctx := context.TODO()
	res := httptest.NewRecorder()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "/promotions/"+id, nil)
	if err != nil {
		t.Fatalf("unable to perform request: %s", err)
	}

	srv.ServeHTTP(res, req)

	want := http.StatusOK
	if res.Code != want {
		t.Fatalf("not expected code want: %d got: %d", want, res.Code)
	}
}
