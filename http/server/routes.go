package server

import (
	"net/http"

	logger "github.com/chi-middleware/logrus-logger"
	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
)

func (h *Handler) handleHealthz(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func (h *Handler) routes() {
	h.router.Get("/healthz", h.handleHealthz)

	log := logrus.New()

	h.router.Route("/promotions", func(r chi.Router) {
		r.Use(logger.Logger("router", log))

		r.With(h.promotionCtx).Route("/{promotionID}", func(r chi.Router) {
			r.Get("/", h.getPromotion)
		})
	})
}
