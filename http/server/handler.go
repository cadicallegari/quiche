package server

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cadicallegari/quiche"
)

type contextKey int32

const (
	contentTypeJSON   = "application/json; charset=UTF-8"
	contentTypeHeader = "Content-Type"

	promotionKey contextKey = iota
)

var ErrInvalidURLPath = fmt.Errorf("unable url path")

type Handler struct {
	router  *chi.Mux
	storage quiche.PromotionStorager
}

func handleError(w http.ResponseWriter, statusCode int, err error) {
	var msg string
	if err != nil {
		msg = fmt.Sprintf(`{"error": %q}`, err)
	}

	http.Error(w, msg, statusCode)
}

func (h *Handler) getPromotion(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	w.Header().Set(contentTypeHeader, contentTypeJSON)

	p, ok := ctx.Value(promotionKey).(*quiche.Promotion)
	if !ok {
		handleError(w, http.StatusInternalServerError, nil)
		return
	}

	response, err := json.Marshal(p)
	if err != nil {
		handleError(w, http.StatusInternalServerError, err)
		return
	}

	w.WriteHeader(http.StatusOK)

	if _, err := w.Write(response); err != nil {
		log.WithError(err).Error("unable to write response")
	}
}

func (h *Handler) promotionCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		promotionID := chi.URLParam(r, "promotionID")

		p, err := h.storage.Get(ctx, promotionID)
		if err != nil {
			if errors.Is(err, quiche.ErrKeyNotFound) {
				handleError(w, http.StatusNotFound, nil)
			} else {
				handleError(w, http.StatusInternalServerError, err)
			}
			return
		}

		ctx = context.WithValue(ctx, promotionKey, p)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func NewPromotionHandler(storage quiche.PromotionStorager) *chi.Mux {
	s := Handler{
		router:  chi.NewRouter(),
		storage: storage,
	}

	s.routes()

	return s.router
}
