// +build integration

package server_test

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/shopspring/decimal"
	"gitlab.com/cadicallegari/quiche"
	"gitlab.com/cadicallegari/quiche/http/server"
	"gitlab.com/cadicallegari/quiche/loader"
	"gitlab.com/cadicallegari/quiche/storage"
)

func TestShouldReturnProperlyUsingSyncer(t *testing.T) {
	ctx := context.TODO()

	stg, err := storage.NewLocalStorage()
	if err != nil {
		t.Fatalf("unable to perform request: %s", err)
	}

	syncEach := 10 * time.Minute

	promotionSyncer, err := loader.NewPromotionFileLoader(stg, "/testdata/promotions-test.csv", syncEach)
	if err != nil {
		t.Fatalf("unable create syncer: %s", err)
	}

	if err := promotionSyncer.Sync(ctx); err != nil {
		t.Fatalf("not expected error running sync: %s", err)
	}

	id := "96d71b4f-9a75-4edf-b36b-66339d8e3b27"
	expirationDate, err := time.Parse(loader.DateLayout, "2018-09-07 13:55:54 +0200 CEST")
	if err != nil {
		t.Fatalf("unable to create expiration: %s", err)
	}

	price, err := decimal.NewFromString("65.016776")
	if err != nil {
		t.Fatalf("unable to create price: %s", err)
	}

	want := quiche.Promotion{
		ID:             id,
		Price:          price,
		ExpirationDate: expirationDate,
	}

	srv := server.NewPromotionHandler(stg)
	res := httptest.NewRecorder()

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "/promotions/"+id, nil)
	if err != nil {
		t.Fatalf("unable to perform request: %s", err)
	}

	wantCode := http.StatusOK
	srv.ServeHTTP(res, req)
	if res.Code != wantCode {
		t.Fatalf("not expected code want: %d got: %d", wantCode, res.Code)
	}

	var got quiche.Promotion

	if err := json.Unmarshal(res.Body.Bytes(), &got); err != nil {
		t.Fatalf("unable to decode response: %s", err)
	}

	if diff := cmp.Diff(want, got); diff != "" {
		t.Errorf("not expecting response (-want +got):\n%s", diff)
	}
}
