package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cadicallegari/quiche/http/server"
	"gitlab.com/cadicallegari/quiche/loader"
	"gitlab.com/cadicallegari/quiche/storage"
)

type Config struct {
	RedisURL      string `envconfig:"REDIS_URL"`
	RedisPassword string `envconfig:"REDIS_PASSWORD"`
	RedisDB       int    `envconfig:"REDIS_DB" default:"3"`

	FileName  string        `envconfig:"FILE_NAME"`
	SyncEvery time.Duration `envconfig:"SYNC_EVERY" default:"30m"`

	Port int `default:"8888"`
}

var version = "dev" // this will be set on build time

func gracefulShutdown(srv *http.Server, ch chan struct{}) {
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)
	<-sigint

	ctx := context.Background()

	if err := srv.Shutdown(ctx); err != nil {
		log.WithError(err).Fatal("unable to shutdown the server properlly")
	}

	close(ch)
}

func main() {
	var cfg Config

	err := envconfig.Process("quiche", &cfg)
	if err != nil {
		log.WithError(err).Fatal("unable to read configs")
	}

	stg, err := storage.NewRedisStorage(
		context.Background(),
		cfg.RedisURL,
		cfg.RedisPassword,
		cfg.RedisDB,
	)
	// to use memory storage or we can even pass some setting via cmd args or env vars
	// stg, err := storage.NewLocalStorage()
	if err != nil {
		log.WithError(err).Fatal("unable to create storage")
	}

	promotionSyncer, err := loader.NewPromotionFileLoader(stg, cfg.FileName, cfg.SyncEvery)
	if err != nil {
		log.WithError(err).Fatal("unable to create loader")
	}

	if err := promotionSyncer.Sync(context.Background()); err != nil {
		log.WithError(err).Fatal("unable to sync")
	}
	defer promotionSyncer.Stop()
	promotionSyncer.Run()

	handler := server.NewPromotionHandler(stg)

	srv := &http.Server{
		Addr:        fmt.Sprintf(":%d", cfg.Port),
		Handler:     handler,
		ReadTimeout: time.Minute,
	}

	done := make(chan struct{})
	go gracefulShutdown(srv, done)

	log.WithFields(log.Fields{
		"version": version,
		"port":    cfg.Port,
	}).Info("server started")

	if err := srv.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
		log.WithError(err).Fatal("serving http")
	}

	<-done
}
