package storage

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/go-redis/redis/v8"
	"gitlab.com/cadicallegari/quiche"
)

type RedisStorage struct {
	client *redis.Client
}

func (s *RedisStorage) Get(ctx context.Context, id string) (*quiche.Promotion, error) {
	raw, err := s.client.Get(ctx, id).Bytes()
	if err != nil {
		if errors.Is(err, redis.Nil) {
			return nil, fmt.Errorf("%w key: %s", quiche.ErrKeyNotFound, id)
		}

		return nil, err
	}

	var p quiche.Promotion

	if err := json.Unmarshal(raw, &p); err != nil {
		return nil, err
	}

	return &p, nil
}

func (s *RedisStorage) Add(ctx context.Context, p *quiche.Promotion) error {
	data, err := json.Marshal(p)
	if err != nil {
		return err
	}

	return s.client.Set(ctx, p.ID, data, 0).Err()
}

func (s *RedisStorage) Del(ctx context.Context, id string) error {
	return s.client.Del(ctx, id).Err()
}

func (s *RedisStorage) Keys(ctx context.Context) ([]string, error) {
	return s.client.Keys(ctx, "*").Result()
}

func NewRedisStorage(ctx context.Context, addr, password string, db int) (*RedisStorage, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: password, // no password set
		DB:       db,       // use default DB
	})

	if _, err := client.Ping(ctx).Result(); err != nil {
		return nil, err
	}

	return &RedisStorage{client: client}, nil
}
