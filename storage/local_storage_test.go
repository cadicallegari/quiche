package storage_test

import (
	"context"
	"errors"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/cadicallegari/quiche"
	"gitlab.com/cadicallegari/quiche/storage"
)

func TestLocalShouldReturnReturnFalseWhenKeyAbsent(t *testing.T) {
	s, err := storage.NewLocalStorage()
	if err != nil {
		t.Errorf("not expected creating storage: %s\n", err)
	}

	_, err = s.Get(context.TODO(), "not_exists")
	if !errors.Is(err, quiche.ErrKeyNotFound) {
		t.Errorf("expecting not found when key not present")
	}
}

func TestLocalShouldAddAndGetKeyProperly(t *testing.T) {
	s, err := storage.NewLocalStorage()
	if err != nil {
		t.Errorf("not expected error: %s\n", err)
	}

	ctx := context.TODO()
	id := "anyID"
	promotion := &quiche.Promotion{
		ID: id,
	}

	_, err = s.Get(ctx, id)
	if !errors.Is(err, quiche.ErrKeyNotFound) {
		t.Errorf("expecting not found got: %s", err)
	}

	if err := s.Add(ctx, promotion); err != nil {
		t.Errorf("not expected error adding promotion: %s", err)
	}

	got, err := s.Get(ctx, id)
	if err != nil {
		t.Errorf("expecting success getting an existing id %q", id)
	}

	if diff := cmp.Diff(promotion, got); diff != "" {
		t.Errorf("not expecting response (-want +got):\n%s", diff)
	}
}
