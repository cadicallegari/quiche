package storage

import (
	"context"
	"fmt"

	"gitlab.com/cadicallegari/quiche"
)

type LocalStorage struct {
	data map[string]*quiche.Promotion
}

func (s *LocalStorage) Get(ctx context.Context, id string) (*quiche.Promotion, error) {
	v, ok := s.data[id]
	if !ok {
		return nil, fmt.Errorf("%w key: %s", quiche.ErrKeyNotFound, id)
	}

	return v, nil
}

func (s *LocalStorage) Add(ctx context.Context, p *quiche.Promotion) error {
	s.data[p.ID] = p

	return nil
}

func (s *LocalStorage) Del(ctx context.Context, id string) error {
	delete(s.data, id)

	return nil
}

func (s *LocalStorage) Keys(_ context.Context) ([]string, error) {
	keys := make([]string, 0, len(s.data))
	for k := range s.data {
		keys = append(keys, k)
	}

	return keys, nil
}

func NewLocalStorage() (*LocalStorage, error) {
	return &LocalStorage{data: make(map[string]*quiche.Promotion)}, nil
}
