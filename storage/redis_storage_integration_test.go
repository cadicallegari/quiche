// +build integration

package storage_test

import (
	"context"
	"errors"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/cadicallegari/quiche"
	"gitlab.com/cadicallegari/quiche/storage"
)

func TestRedisShouldReturnReturnFalseWhenKeyAbsent(t *testing.T) {
	ctx := context.TODO()
	s, err := storage.NewRedisStorage(ctx, os.Getenv("QUICHE_REDIS_URL"), "", 0)
	if err != nil {
		t.Errorf("not expected creating storage: %s", err)
	}

	_, err = s.Get(context.TODO(), "not_exists")
	if !errors.Is(err, quiche.ErrKeyNotFound) {
		t.Errorf("expecting not found when key not present")
	}
}

func TestRedisShouldAddAndGetKeyProperly(t *testing.T) {
	ctx := context.TODO()
	s, err := storage.NewRedisStorage(ctx, os.Getenv("QUICHE_REDIS_URL"), "", 0)
	if err != nil {
		t.Errorf("not expected error: %s", err)
	}

	id := t.Name()
	promotion := &quiche.Promotion{
		ID: id,
	}

	_, err = s.Get(ctx, id)
	if !errors.Is(err, quiche.ErrKeyNotFound) {
		t.Error("expecting not found")
	}

	if err := s.Add(ctx, promotion); err != nil {
		t.Errorf("not expected error adding promotion: %s", err)
	}
	defer s.Del(ctx, id)

	got, err := s.Get(ctx, id)
	if errors.Is(err, quiche.ErrKeyNotFound) {
		t.Errorf("expecting success getting an existing id %q", id)
	}

	if diff := cmp.Diff(promotion, got); diff != "" {
		t.Errorf("not expecting response (-want +got):\n%s", diff)
	}
}
