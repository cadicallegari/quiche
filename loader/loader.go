package loader

import (
	"context"
	"encoding/csv"
	"io"
	"os"
	"sync/atomic"
	"time"

	"github.com/shopspring/decimal"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cadicallegari/quiche"
)

type FilePromotionLoader struct {
	running   int32
	stop      chan struct{}
	storage   quiche.PromotionStorager
	filename  string
	syncEvery time.Duration
}

const DateLayout = "2006-01-02 15:04:05 -0700 MST"

func (ps *FilePromotionLoader) Run() {
	if !atomic.CompareAndSwapInt32(&ps.running, 0, 1) {
		// it's already running or stopped
		return
	}

	go func() {
		for {
			select {
			case <-time.After(ps.syncEvery):
			case <-ps.stop:
				return
			}

			log.Info("start sync from file")

			ctx := context.Background()

			err := ps.Sync(ctx)
			if err != nil {
				log.WithError(err).Error("unable to sync from file")
			}
		}
	}()
}

func (ps *FilePromotionLoader) Sync(ctx context.Context) error {
	oldKeys := make(map[string]bool)

	keys, err := ps.storage.Keys(ctx)
	if err != nil {
		return err
	}

	for _, k := range keys {
		oldKeys[k] = true
	}

	file, err := os.Open(ps.filename)
	if err != nil {
		return err
	}
	defer file.Close()

	r := csv.NewReader(file)

	for {
		record, err := r.Read()
		if err == io.EOF {
			if err := ps.removeKeys(ctx, oldKeys); err != nil {
				return err
			}

			break
		}

		if err != nil {
			return err
		}

		price, err := decimal.NewFromString(record[1])
		if err != nil {
			return err
		}

		expirationDate, err := time.Parse(DateLayout, record[2])
		if err != nil {
			return err
		}

		p := &quiche.Promotion{
			ID:             record[0],
			Price:          price,
			ExpirationDate: expirationDate,
		}

		delete(oldKeys, p.ID)

		if err := ps.storage.Add(ctx, p); err != nil {
			return err
		}
	}

	return nil
}

func (ps *FilePromotionLoader) removeKeys(ctx context.Context, keys map[string]bool) error {
	for k := range keys {
		if err := ps.storage.Del(ctx, k); err != nil {
			return err
		}
	}

	return nil
}

func (ps *FilePromotionLoader) Stop() {
	atomic.SwapInt32(&ps.running, 2)
	ps.stop <- struct{}{}
	close(ps.stop)
}

func NewPromotionFileLoader(
	stg quiche.PromotionStorager,
	filename string,
	syncEvery time.Duration,
) (*FilePromotionLoader, error) {
	return &FilePromotionLoader{
		stop:      make(chan struct{}),
		storage:   stg,
		filename:  filename,
		syncEvery: syncEvery,
	}, nil
}
