package quiche

import (
	"context"
	"errors"
	"time"

	"github.com/shopspring/decimal"
)

var ErrKeyNotFound = errors.New("key not found")

type (
	Promotion struct {
		ID             string          `json:"id"`
		Price          decimal.Decimal `json:"price"`
		ExpirationDate time.Time       `json:"expiration_date"`
	}

	PromotionStorager interface {
		Get(_ context.Context, key string) (*Promotion, error)
		Add(_ context.Context, _ *Promotion) error
		Del(_ context.Context, id string) error
		Keys(_ context.Context) ([]string, error)
	}

	PromotionLoader interface {
		Run()
		Sync(_ context.Context) error
		Stop()
	}
)
